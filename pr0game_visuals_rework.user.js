// ==UserScript==
// @name         Pr0game Visual Rework
// @namespace    http://tampermonkey.net/
// @version      1.01
// @description  A little visuals rework of pr0game, adding transparency, custom background, menu button animation, playercard opening by clicking on your name "Imperator XXX"
// @author       Ceeser
// @match        https://pr0game.com/*
// @icon         https://pr0game.com/favicon.ico
// @grant        GM_addStyle
// ==/UserScript==
/* globals Dialog */
(function() {
    'use strict';

    // Resourcesuntilwhen script class
    GM_addStyle('#atain { padding: 25px 0px 0px 16px; }');

    // Custom background. Replace the URL with any background image you want
    GM_addStyle('html { background-image: url("https://files.wallpaperpass.com/2019/10/universe%20wallpaper%20048%20-%201920x1080.jpg");}');

    //Others - feel free to play around with colors/transparency
    GM_addStyle('td { background: #2a2e31aa; }');
    GM_addStyle('th { background: #0a0e11aa; }');
    GM_addStyle('table { background: #0a0e11aa; }');
    GM_addStyle('top .fixed { border-bottom-left-radius: 100px; border-bottom-right-radius: 100px; }');
    GM_addStyle('header .fixed { background: #00000000; }');

    GM_addStyle('.infos { background: #2a2e31cc; }');
    GM_addStyle('.planeto { background: #111111aa; }');
    GM_addStyle('.planeth img { margin: 0 10px 20px 10px; }');
    GM_addStyle('.buildl { background: #111111aa; }');
    GM_addStyle('.buildn { background: #ee4d2edd; }');
    GM_addStyle('.transparent { background: #00000000; }');

    GM_addStyle('#menu { padding: 25px 0px 0px 14px; max-width: 180px; }');
    GM_addStyle('#menu a { background: #2a2e31aa; padding: 4px 4px; width: 160px }');
    GM_addStyle('#menu a:hover {  background-color: #666666aa; background-image: box-shadow: 0 0 0 1px #000000 inset,0 0 0 2px rgba(255, 255, 255, 0.15) inset,0 4px 0 0 #333333,0 4px 0 1px rgba(0, 0, 0, 0.4),0 4px 4px 1px rgba(0, 0, 0, 0.5); color: white !important; display: block; font-family: Arial, sans-serif; font-size: 18px; letter-spacing: 1px; position: relative; text-align: center; text-shadow: 0 1px 1px rgba(0, 0, 0, 0.5); text-decoration: none !important; -webkit-transition: all .2s linear; }');

    function openPlayerCard() {
        return Dialog.Playercard(get_own_id()); // You can replace get_own_id() with your own userID
    }

    var links = document.getElementsByTagName("a");

    for (var i=0,imax=links.length; i<imax; i++) {
        if (links[i].href == "https://pr0game.com/game.php?page=settings") {
            links[i].href = "#";
            links[i].addEventListener("click", openPlayerCard);

            break; // only replace the first at the header
        }
    }

    // Thank you Atain for the following functions
    function store_playerid(){
        if(document.getElementById("who")==null){
            return
        }
        if(document.getElementById("who").value!="1" || document.querySelector("a[style='color:lime'")==null){
            return
        }

        localStorage.ownid=document.querySelector("a[style='color:lime'").onclick.toString().match(/\(\d+,/g)[0].substring(1).replace(",","")
    }

    function get_own_id(){
     if(localStorage.ownid==null){
      return 1
     }
        return localStorage.ownid
    }

    store_playerid();
})();