// ==UserScript==
// @name         Buildtimer
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  zeigt Zeit bis Bau möglich ist bei den Gebäudekosten an
// @author       Siera
// @match        https://pr0game.com/uni2/*
// @icon         https://pr0game.com/favicon.ico
// @updateURL    https://codeberg.org/pr0game/userscripts/raw/branch/master/Buildtimer.user.js
// @downloadURL  https://codeberg.org/pr0game/userscripts/raw/branch/master/Buildtimer.user.js
// @supportURL   https://codeberg.org/pr0game/userscripts/issues
// @grant        GM_setValue
// @grant        GM_getValue
// ==/UserScript==

function isImperiumPage() {
  return window.location.href === "https://pr0game.com/uni2/game.php?page=imperium";
}

function isBuildingsPage() {
  return window.location.href === "https://pr0game.com/uni2/game.php?page=buildings";
}

function getOptionTexts(selectElement) {
  // Initialisiere ein leeres Array, in das wir die Option-Texte speichern werden
  let optionTexts = [];

  // Iteriere über alle Optionen im Select-Element
  for (let option of selectElement.options) {
    // Füge den Option-Text zum Array hinzu
    optionTexts.push(option.text);
  }

  // Gib das Array mit den Option-Texten zurück
  return optionTexts;
}

function createObjectsFromOptionTexts(optionTexts) {
  // Initialisiere ein leeres Array, in das wir die Objekte speichern werden
  let objects = [];

  // Iteriere über alle Option-Texte
  for (let optionText of optionTexts) {
    // Zerteile den Option-Text anhand des Leerzeichens
    let parts = optionText.split(' ');

    // Extrahiere die Nummer des Planeten aus dem Option-Text
    let planetNumber = parts[1].slice(1, -1);

    // Erstelle ein Objekt mit der Nummer des Planeten als Schlüssel und einem Wert mit metallPH, kristallPH und deutPH als 0
    let object = {
      [planetNumber]: {
        metallPH: 0,
        kristallPH: 0,
        deutPH: 0
      }
    };

    // Füge das Objekt dem Array hinzu
    objects.push(object);
  }

  // Gib das Array mit den Objekten zurück
  return objects;
}

function getPlanetNumber(table, rowIndex, cellIndex) {
  // Hole die angegebene Zeile aus der Tabelle
  let row = table.rows[rowIndex];

  // Hole die angegebene Zelle aus der Zeile
  let cell = row.cells[cellIndex];

    // Gib den Inhalt der Zelle zurück
    // Zerteile den Text anhand der Zeichen '[' und ']'
    let parts = cell.innerHTML.split(/\[|\]/);

    // Der zweite Teil enthält die gewünschte Nummer
    let number = parts[1].replace('\n', '');
    number = number.replace(/\s+/g, '');
    //console.log("nummer in fkt getplanetnumber: "+number);
  return number;
}

function getPlanetaryProduction(){
    const ROW_PLANETNUMBER = 3;
    const ROW_METALL = 6;
    const ROW_KRISTALL = 7;
    const ROW_DEUT = 8;

    let selectElement = document.getElementById('planetSelector');
    let optionTexts = getOptionTexts(selectElement); //optionText enthält alle vorhandenen Planetennummern [4:320:6], usw.
    //console.log(optionTexts[0]);
    let listeAllerPlanetenProduktionen = createObjectsFromOptionTexts(optionTexts); //Array von Objekten mit Planetennummern als Keys in denen metallPH, kristallPH, und deutPH gespeichert werden können
    let maximalerZeilenIndex = selectElement.options.length + 1; //abhängig von der Anzahl der Planeten kann bis zu diesem Index auf der Imperiums Seite durch die Zeile iteriert werden
    //console.log(maximalerZeilenIndex);
    let table = document.querySelector("content table");
    let rows = table.querySelectorAll("tr");

    for (let i = 2; i <= maximalerZeilenIndex; i++) {
        let planetNumber = getPlanetNumber(table, ROW_PLANETNUMBER, i);
        //console.log(planetNumber);
        let productionCellMet = rows[ROW_METALL].querySelectorAll("td")[i];
        let productionText = productionCellMet.innerHTML;
        let textTeile = productionText.split('<br>');
        //console.log("textTeile1: " + textTeile[1]);
        let metPHmitPunkt = textTeile[1].match(/\d+(\.\d+)?/);
        //console.log("metPHmitPunkt: "+ metPHmitPunkt);
        let metPHString = metPHmitPunkt.toString();
        let planet = listeAllerPlanetenProduktionen.find(i => Object.keys(i)[0] === planetNumber);
        planet[planetNumber].metallPH = parseInt(metPHString.replace('.', ''), 10);
        //console.log("planet.metPH: "+planet[planetNumber].metallPH);

        let productionCellKris = rows[ROW_KRISTALL].querySelectorAll("td")[i];
        productionText = productionCellKris.innerHTML;
        textTeile = productionText.split('<br>');
        //console.log("textTeile1: " + textTeile[1]);
        let krisPHmitPunkt = textTeile[1].match(/\d+(\.\d+)?/);
        //console.log("metPHmitPunkt: "+ metPHmitPunkt);
        let krisPHString = krisPHmitPunkt.toString();
        planet = listeAllerPlanetenProduktionen.find(i => Object.keys(i)[0] === planetNumber);
        planet[planetNumber].kristallPH = parseInt(krisPHString.replace('.', ''), 10);
        //console.log("planet.metPH: "+planet[planetNumber].metallPH);

        let productionCellDeut = rows[ROW_DEUT].querySelectorAll("td")[i];
        productionText = productionCellDeut.innerHTML;
        textTeile = productionText.split('<br>');
        //console.log("textTeile1: " + textTeile[1]);
        let deutPHmitPunkt = textTeile[1].match(/\d+(\.\d+)?/);
        //console.log("deutPHmitPunkt: "+ deutPHmitPunkt);
        let deutPHString = deutPHmitPunkt.toString();
        planet = listeAllerPlanetenProduktionen.find(i => Object.keys(i)[0] === planetNumber);
        planet[planetNumber].deutPH = parseInt(deutPHString.replace('.', ''), 10);
        //console.log("planet.metPH: "+planet[planetNumber].metallPH);


        //console.log("metPH aus ArrayObejkt: "+JSON.stringify(listeAllerPlanetenProduktionen));

         GM_setValue("produktion", listeAllerPlanetenProduktionen);
    }
}

function extraktKosten(element){
    let uebrigeKosten = {metall: 0, kristall: 0, deut: 0};
    let text = element.innerText;
    uebrigeKosten.metall = parseInt(text.split(": ")[2].replace(".", ""));//.replace(",", ""));
    if (text.includes("Kristall")) {
        uebrigeKosten.kristall = parseInt(text.split(": ")[3].replace(".", ""));//.replace(",", ""));
    }
    if (text.includes("Deuterium")) {
        uebrigeKosten.deut = parseInt(text.split(": ")[4].replace(".", ""));//.replace(",", ""));
    }
    //console.log(uebrigeKosten.deut);
    return uebrigeKosten;
}

function calculateCostCoverTime(productionPerHour, remainingCosts) {
  const selectedOption = document.querySelector('#planetSelector').selectedOptions[0];
  const key = selectedOption.textContent.match(/\[(.*?)\]/)[1];
  const selectedItem = productionPerHour.find(item => Object.keys(item)[0] === key);

  if (!selectedItem) {
    return { time: 0, resource: '' };
  }

  let longestTime = 0;
  let longestResource = '';

  for (const resource in remainingCosts) {
    const resourceCost = remainingCosts[resource];
    const totalProduction = selectedItem[key][`${resource}PH`];

    const resourceTime = resourceCost / totalProduction * 60;
    if (resourceTime > longestTime) {
      let roundedTime = resourceTime.toFixed(0);
      longestTime = parseInt(roundedTime, 10);
      longestResource = resource;
    }
  }

  return { time: longestTime, resource: longestResource };
}

function firstLetterToUpperCase(string){
    let firstChar = string.charAt(0).toUpperCase();
    return firstChar + string.slice(1);
}

function minutesToHoursAndMinutes(minutes) {
  const hours = Math.floor(minutes / 60);
  const remainingMinutes = minutes % 60;
  const currentDate = new Date();
  currentDate.setMinutes(currentDate.getMinutes() + minutes);

  return {
    hours: hours,
    minutes: remainingMinutes,
    time: currentDate.toLocaleTimeString(),
  };
}


(function() {
    'use strict';
    window.addEventListener("load", function(){
        if (isImperiumPage()) {
            //console.log("isImperiumPage");
            getPlanetaryProduction();
          
        } else if (isBuildingsPage()) {
            let produktion = GM_getValue("produktion", 0);
            for (let i = 0; i < document.getElementsByClassName('buildl').length; i = i+2){
                let ele = document.getElementsByClassName('buildl')[i];

                let uebrigeKosten = extraktKosten(ele);

               /* console.log("ProduktionsArray: "+JSON.stringify(produktion));
                console.log("UebrigeKostenArray: "+JSON.stringify(uebrigeKosten));
                console.log("kalkulation: "+JSON.stringify(calculateCostCoverTime(produktion, uebrigeKosten)));*/
                let prod = calculateCostCoverTime(produktion, uebrigeKosten);

                if (prod.time === 0) {
                    ele.innerHTML = ele.innerHTML + "Bau möglich!";
                } else {
                    let zeit = minutesToHoursAndMinutes(prod.time);
                    ele.innerHTML = ele.innerHTML + "Genug "+ firstLetterToUpperCase(prod.resource) +" in: " + zeit.hours + " Stunden und " + zeit.minutes + " min. Bau möglich um " + zeit.time + " Uhr.";
                }
            }
        }

            }, false)
})();