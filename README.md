# lazy_pr0gamer Userscripts

Userscripts um pr0game einfacher zu gestalten

## Installation

1. Tampermonkey (**es funktioniert NICHT mit Greasemonkey!**) installieren:
    * [Chrome]
    * [Firefox]
    * [Edge]
    * [Firefox for Android]
2. Goldgepresstes Latinum an mich schicken

> **Achtung: ich teste nur mit Tampermonkey unter Chrome**

## Userscripts

| Userscript                                | Author             | Aktion                              |
|-------------------------------------------|-------------------|-------------------------------------|
| [lazy_pr0gamer](#lazy_pr0gamer)           | [atain]           | [lazy_pr0gamer installieren]        |
| [Galaxy Control](#galaxy-control)         | [AxelFLOSS]       | [Galaxy Control installieren]       |
| [Visuals](#visuals)                       | [AxelFLOSS]       | [Visuals installieren]              |
| [Space Background](#space-background)     | [AxelFLOSS]       | [Space Background installieren]     |
| [Scavengers Toolbox](#scavengers-toolbox) | [AxelFLOSS] / [Timo_Ka]      | [Scavengers Toolbox installieren]   |
| [Gimme Charts](#gimme-charts)             | joghurtrucksack   | [Gimme Charts installieren]         |
| [Warsim](#warsim)                         | joghurtrucksack   | [Warsim installieren]               |
| [Raidstats](#raidstats)                   | [reflexrecon]     | [Raidstats installieren]            |
| [Exchange Planet Images](#exchange-planet-images)                     | eichhorn / [Timo_Ka]     | [Exchange Planet Images installieren]            |
| [Mission align](#mission-align)           | [DawnOfTheUwe]    | [Mission align installieren]        |
| [Buildtimer](#buildtimer)                 | [Siera]           | [Buildtimer installieren]        |

### lazy_pr0gamer

**Features**

Rohstoffe:
- Rohstoffrechner basierend auf der eigenen Produktion + auf den Planeten
- Anzeige in wie vielen stunden das nächste Lager auf allen Planeten voll ist und welcher Rohstoff

Flotten:
- Buttons für volle gt expos mit oder ohne bestes Schiff
- Anzeige von Flottenankunft beim 2. Flottenmenü
- Uhrzeit in der Übersicht zur relativen Zeit in Stunden

Expos:
- max. Expo-Punkte und aktuell ausgewählte Punkte im Flottenmenü
- selbst eingestellte Expo-Flotten
- Fundgröße in den Nachrichten
- Statistiken über Expos 
- Anzahl an Expos zu dem ausgewähltem Ziel in den letzten 24 Stunden im 2. Flottenmenü

**Funktionshinweise**

- nach jedem Minenupgrade den Rohstofftab von dem Planeten anschauen
- nach jeder Maximiererforschung alle Planeten anschauen
- ab und zu mal schauen, ob nero die nächste Stufe für max Expo-Punkte erreicht hat (Statistikpunkte Platz 1-100 anschauen)
- Rohstoffe die Flotten haben werden nicht berücksichtigt - wenn Flotten Rohstoffe transferiert haben auf einen anderen Planeten, diesen anschauen
- wenn Lager ausgebaut wurden auf den Planeten gehen (egal welche Ansicht)
- nach Expos die Berichte und die Rückkehr von Rohstoffen anschauen
- niemanden der Handelsförderation angreifen (sehr wichtig)

**Probleme/Updates**

Zum Updaten auf das Tampermonkeysymbol klicken und dann auf "Hilfsmittel", dort dann nach Updates suchen anklicken.

Wenn Planeten aufgegeben wurden, die nicht wieder kolonisiert werden oder man auf Englisch spielt und deswegen die Statistik nicht passt von den Expos: 

- `Tampermonkey` > `Übersicht` > `Einstellungen`
- `Konfigurations-Modus` auf `Experte` stellen (ist ganz oben) 
- `Installierte Userscripte` > `progame_resourcetilwhen` > `Speicher`
- in dem Textfeld alles rauslöschen und `{}` eingeben, dann speichern

Wenn immernoch ein Fehler auftritt, bei mir über Discord melden.

### Galaxy Control

Erlaubt die Bedienung der Galaxieansicht über Pfeiltasten und WASD.

| Befehl            | Taste    |
|-------------------|----------|
| Vorheriges System | `←`, `a` |
| Nächstes System   | `→`, `d` |
| Vorherige Galaxie | `↑`, `w` |
| Nächste Galaxie   | `↓`, `s` |

### Visuals

Kleine grafische Verbesserungen, wie bspw. keine Verschiebungen mehr im UI beim Hovern über Links durch Größenänderung des Textes.

### Space Background

Ersetzt das Hintergrundbild im Spiel.

![screenshot_space_background](media/screenshot_background.png)

### Scavengers Toolbox

Ergänzt den Spionagereport um Werte für:

* Gesamtzahl Ressourcen
* Gefahrenpotenzial (Anzahl Schiffe * Angriffswert) mit farblicher Hervorhebung wenn größer 0
* Notwendige Kleine Transporter (voerst: (Potenziell zu erbeutende Ressourcen / 5000) + 1)
* Recyclepotenzial entspricht dem potenziellen Trummerfeld
* Potenziell zu erbeutende Ressourcen
* Potenziell notwendige Recycler
* Best Ress / Sek zeigt die optimale Ressausbeute pro Sekunde an
* Best Plant zeigt dazu den besten Startpunkt des Raids, Sonderfall Galasprung hier wird "Bestmöglicher Planet in Gala X" angezeigt
* Marktwert der Ressourcen entspricht dem Gegenwert gemäß Marktplatz ( eigene Werte können im Marktplatz gepflegt werden)
* Ress pro Sekunde Flugzeit gibt an wieviele Ressourcen pro sekunde von dem aktuell aktiven Planeten erbeutet werden können

Wenn "Ressourcen mit X KTs abholen" rot hinterlegt ist wird der Planet verteidigt.

Um das Script benutzten zu können müssen erst die Seiten "Marktplatz" und "Forschung" aufgerufen werden, ansonsten wird folgender Fehler angezeigt:

![Scavengers_Toolbox_No_Techs](media/screenshot_scavengers_toolbox_no_techs.png)

Das Farbschema schlüsselt sich wie folgt auf, die Farben sind jedoch gedeckt gehalten:
* rot = schlecht
* orange = mittel
* grün = gut
* blau = optimal

![Scavengers_Toolbox_Farben_schlecht_mittel](media/screenshot_scavengers_toolbox_colors_1.png)
![Scavengers_Toolbox_Farben_gut_optimal](media/screenshot_scavengers_toolbox_colors_2.png)



### Gimme Charts

Zeigt Statistiken zu deinen Expos in Diagrammform an.
Es werden alle Expos erfasst, deren Nachricht einmal nach Installation des Skripts aufgerufen wird.
Das Skript blendet automatisch auf der Expo Nachrichten Seite eine Grafik oberhalb des Nachrichtenfensters ein.

Momentan sind u.A. folgende Diagramme vorhanden und koennen ueber das dropdown Feld oberhalb des Diagramms gewechselt werden.

![ein Diagramm das die taeglichen Resourcengewinne durch Expos zeigt](media/charts_daily.png)

![ein Diagramm das die Aufschluesselung aller Expos nach Eventart zeigt](media/charts_events.png)

In der Zukunft gibt es vielleicht auch Grafiken zu andere Dingen.

#### Warsim

Setzt in neuen Kampfsimulatoren die Anzahl fuer nicht-Kampfschiffe auf 0.
Klick auf den Schiffnamen in der Zeile setzt den Wert zurueck auf den Maximalwert.

Betroffene Schiffe:

- Transporter
- Recycler
- Kolonieschiff
- Spiosonden
- Todesstern

#### Raidstats
Zeigt Statistiken zu deinen Kampfberichten in Diagrammform an.
Aktuell nur Metall, Kristall und Deuterium Gewinne pro Tag.
![ein Diagramm das die taeglichen Resourcengewinne durch Kämpfe zeigt](media/raid_daily.png)

#### Exchange Planet Images
Made by eichhorn.

Planetenbilder sind in den Einstellungen individuell einstellbar.

![screenshot_space_background](media/Exchange_Planet_Images.png)

#### Mission align

Kleine Verbesserung bei der Formatierung von Missionen in der Übersicht und der Phalanx-Ansicht.

Missionen sind in der Übersicht einzeilig und linksbündig:

![screenshot_missions_overview](media/mission_overview.png)

Missionen sind in der Phalanx linksbündig:

![screenshot_missions_phalanx](media/mission_phalanx.png)


#### Buildtimer
Einfaches Skript das die Produktion pro Stunde aus der Imperiumsseite ausliest
und im Baumenü anzeigt, wie lange es noch dauert bis ein Gebäude (mit der Produktion des Planeten) gebaut werden kann. 
![screenshot_Buildtimer](media/Buildtimer.png)


<!-- Link list to unclutter the text -->
[Chrome]: https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=de
[Firefox]: https://addons.mozilla.org/de/firefox/addon/tampermonkey/
[Edge]: https://microsoftedge.microsoft.com/addons/detail/tampermonkey/iikmkjmpaadaobahmlepeloendndfphd
[Firefox for Android]: https://enux.pl/article/en/2021-03-14/how-use-tampermonkey-firefox-mobile
[atain]: https://codeberg.org/atain
[AxelFLOSS]: https://codeberg.org/AxelFLOSS
[Timo_Ka]: https://codeberg.org/Timo_Ka
[reflexrecon]: https://codeberg.org/reflexrecon
[DawnOfTheUwe]: https://codeberg.org/DawnOfTheUwe
[Siera]: https://codeberg.org/Siera
[lazy_pr0gamer installieren]: https://codeberg.org/pr0game/userscripts/raw/branch/master/progame_resourcetilwhen.user.js
[Galaxy Control installieren]: https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_galaxy_control.user.js
[Visuals installieren]: https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_visuals.user.js
[Space Background installieren]: https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_space_background.user.js
[Scavengers Toolbox installieren]: https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_scavengers_toolbox.user.js
[Gimme Charts installieren]: https://codeberg.org/pr0game/userscripts/raw/branch/master/gimmecharts.user.js
[Warsim installieren]: https://codeberg.org/pr0game/userscripts/raw/branch/master/warsim.user.js
[Raidstats installieren]: https://codeberg.org/pr0game/userscripts/raw/branch/master/raidstats.user.js
[Exchange Planet Images installieren]: https://codeberg.org/pr0game/userscripts/raw/branch/master/exchange_planet_images.user.js
[Mission align installieren]: https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_missions.user.js
[Buildtimer installieren]: https://codeberg.org/pr0game/userscripts/raw/branch/master/Buildtimer.user.js