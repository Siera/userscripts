// ==UserScript==
// @name         pr0game Space Background
// @namespace    https://pr0game.pebkac.me/
// @version      0.2
// @description  Adds a nice background image to the game.
// @author       AxelFLOSS
// @match        https://pr0game.com/*
// @updateURL    https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_space_background.user.js
// @downloadURL  https://codeberg.org/pr0game/userscripts/raw/branch/master/pr0game_space_background.user.js
// @supportURL   https://codeberg.org/pr0game/userscripts/issues
// @icon         https://pr0game.com/favicon.ico
// @grant        GM_addStyle
// ==/UserScript==

(function() {
    'use strict';

    GM_addStyle(`
        html {
            animation: movingbg 10s linear infinite;
        }
        @media only screen and (max-width: 640px) {
            html {
                background-image: url("https://s6.imgcdn.dev/GdLkH.jpg");
            }
        }
        @media only screen and (min-width: 641px) and (max-width: 1280px) {
            html {
                background-image: url("https://s6.imgcdn.dev/GdbvS.jpg");
            }
        }
        @media only screen and (min-width: 1281px) {
            html {
                background-image: url("https://s6.imgcdn.dev/GdeFC.jpg");
            }
        }
        @keyframes movingbg {
            from {
                background-size: 102%;
            }
            50% {
                background-size: 102.5%;
            }
            to {
                background-size: 102%;
            }
        }
    `);
})();
